﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicDetection : MonoBehaviour
{
    [SerializeField]
    private FieldOfView fieldOfView;

    //private GameObject[] detectedObjects; //array/or list to make it dynamic, allowing bodies to be detected

    [SerializeField] private float Fieldofvision = 50f;
    [SerializeField] private float Viewdistance = 80f;

    [SerializeField] private GameObject player;

    public bool isPlayerdetected = false;
    // Start is called before the first frame update
    void Start()
    {
        fieldOfView = GetComponentInChildren<FieldOfView>();
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        //fieldOfView.SetOrigin(transform.position);
        fieldOfView.SetAimDirection(Vector3.forward);
        fieldOfView.SetviewDistance(Viewdistance);
        fieldOfView.SetFov(Fieldofvision);
    }

    private void DetectPlayer() //lets do it with math
    {
        if (Vector3.Distance(player.transform.position, transform.position) < Viewdistance)
        {
            Vector3 dirtoPlayer = (player.transform.position - transform.position).normalized;
            if (Vector3.Angle(Vector3.forward, dirtoPlayer) < Fieldofvision/2f)
            {
                isPlayerdetected = true;
            }
            else
            {
                isPlayerdetected = false;
            }

        }
    }

}
