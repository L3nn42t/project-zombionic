﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicHealth : MonoBehaviour
{
    public int _health;
    //[SerializeField] private Player _player //placeholder for player upgrades
    private BasicMovment _movment;
    // Start is called before the first frame update
    void Start()
    {
        _movment = GetComponent<BasicMovment>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_health <= 0)
        {
            Death();
        }
    }

    private void Death()
    {
        _movment._ismoving = false;
        Debug.Log(name + "is dead");
        Despawn();
    }

    private void Despawn() //placeholder if corpses should be destroyed
    {
        Destroy(this.gameObject, 5f);
    }
}
