﻿using Pathfinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicMovment : MonoBehaviour
{

    public float _movespeed;// to be used in other scripts, like abilities
    [SerializeField] private float _nextWaypointDistance = 1f;
    public bool _ismoving;
    [SerializeField] private GameObject _targetobject;
    public bool _seeking = true;

    //pathfinding stuff
    private Path path;
    private int _currentWaypoint = 0;
    private bool _reachedEndOfPath = false;
    private Seeker seeker;


    public Vector3 _targetposition;

    // Start is called before the first frame update
    void Start()
    {
        seeker = GetComponent<Seeker>();
        ActualiseTarget();
        seeker.StartPath(this.gameObject.transform.position, _targetposition, OnPathComplete);
        
        InvokeRepeating("UpdatePath", 0f, 0.5f);
        
    }

    // Update is called once per frame
    void Update()
    {
        if (_ismoving == true)
        {
            Movment();
            
        }
    }
    private void OnPathComplete(Path p)
    {
        if (!p.error)
        {
            path = p;
        }
    }
    private void UpdatePath()
    {
        if (seeker.IsDone() && _reachedEndOfPath == false)
            seeker.StartPath(this.gameObject.transform.position, _targetposition, OnPathComplete);
        if (_seeking == true)
        {
            ActualiseTarget();
        }
        
    }

    private void ActualiseTarget() //gets 
    {
        _targetposition = Position();
    }

    private Vector3 Position()
    {
        Vector3 tpos = _targetobject.transform.position;

        return tpos;
    }

    public void Movment()
    {
        if (path == null)
            return;

        if (_currentWaypoint >= path.vectorPath.Count)
        {
            //_reachedEndOfPath = true;
            
        }
        else
        {
            _reachedEndOfPath = false;

            Vector3 _direction = ((Vector3)path.vectorPath[_currentWaypoint] - this.gameObject.transform.position).normalized;
            Vector3 _movment = _direction * _movespeed * Time.deltaTime;
            float _distance = Vector3.Distance(this.gameObject.transform.position, path.vectorPath[_currentWaypoint]);

            this.gameObject.transform.position += _movment;

            if (_distance < _nextWaypointDistance)
            {
                _currentWaypoint++;
            }
        }
       
        
    }
}
