﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestAI : MonoBehaviour
{
    private BasicMovment movment;
    private BasicDetection detection;
    private BasicHealth health;

    private enum State
    {
        Idle,
        Fleeing,

    }


    
    void Start()
    {
        movment = GetComponent<BasicMovment>();
        detection = GetComponent<BasicDetection>();
        health = GetComponent<BasicHealth>();
    }

    // Update is called once per frame
    void Update()
    {
        if (detection.isPlayerdetected == true && movment._ismoving == false)
        {
            movment._ismoving = true;
        }
    }
}
