﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheapDeath : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            BasicHealth chealth = GetComponent<BasicHealth>();
            chealth._health = 0;
        }
    }
}
