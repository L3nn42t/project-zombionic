﻿using UnityEngine;

public class FieldOfView : MonoBehaviour
{
    [SerializeField] private LayerMask _mask;
    [SerializeField]
    private Mesh _mesh;
    [SerializeField]
    private float _fieldofvision;
    private Vector3 _origin;
    private float _startingangle;
    float viewdistance = 80f;

    //public Transform lookpoint;

    // Start is called before the first frame update
    void Start()
    {
        _mesh = new Mesh();
        GetComponent<MeshFilter>().mesh = _mesh;

        _fieldofvision = 90f;
        _origin = Vector3.zero;
        

    }

    private void LateUpdate()
    {
        int _rayCount = 50;
        float angle = _startingangle;
        float angleincrease = _fieldofvision / _rayCount;
        //viewdistance = 60f;
       

        Vector3[] _verticies = new Vector3[_rayCount + 1 + 1];
        Vector2[] _uv = new Vector2[_verticies.Length];
        int[] _triangels = new int[_rayCount * 3];

        
        _verticies[0] = _origin;

        int vertexIndex = 1;
        int triangleIndex = 0;

        for (int i = 0; i < _rayCount; i++)
        {
            Vector3 vertex;
            RaycastHit ray;
            
            //RaycastHit ray = Physics.Raycast(_origin, GetVectorFromAngle(angle));
            
            RaycastHit2D raycastHit2D = Physics2D.Raycast(_origin, GetVectorFromAngle(angle), viewdistance, _mask);
            if (raycastHit2D.collider == null)
            {
                //No Hits
                vertex = _origin + GetVectorFromAngle(angle) * viewdistance;
            }
            else
            {
                //hit something
                vertex = raycastHit2D.point;
            }

            _verticies[vertexIndex] = vertex;

            if (i > 0)
            {
                _triangels[triangleIndex + 0] = 0;
                _triangels[triangleIndex + 1] = vertexIndex - 1;
                _triangels[triangleIndex + 2] = vertexIndex;

                triangleIndex += 3;
            }

            vertexIndex++;
            angle -= angleincrease;

        }

        _mesh.vertices = _verticies;
        _mesh.uv = _uv;
        _mesh.triangles = _triangels;



    }

    public void SetOrigin(Vector3 Origin)
    {
        Origin = _origin;
    }

    public void SetAimDirection(Vector3 Aimdirecion)
    {
        _startingangle = GetAnglefromVectorfloat(Aimdirecion) -_fieldofvision / 2f;
    }

    public void SetFov(float fov)
    {
        _fieldofvision = fov;
    }

    public void SetviewDistance(float view)
    {
        viewdistance = view;
    }

    public static Vector3 GetVectorFromAngle(float angle)
    {
        float anglerad = angle * (Mathf.PI / 180f);
        return new Vector3(Mathf.Cos(anglerad), Mathf.Sin(anglerad));
    }

    public static float GetAnglefromVectorfloat(Vector3 dir)
    {
        dir = dir.normalized;
        float n = Mathf.Atan2(dir.y, dir.x) * Mathf.Deg2Rad;
        if (n < 0) n += 360;

        return n;
    }

  
}
