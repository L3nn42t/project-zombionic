﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{
    public Transform playerTransform;
    public float smooth = 0.3f;

    private Vector3 velocity = Vector3.zero;

    private void Update()
    {
        Vector3 position = new Vector3();
        position.x = playerTransform.position.x;
        position.z = playerTransform.position.z;
        position.y = playerTransform.position.y +90f;
        transform.position = Vector3.SmoothDamp(transform.position, position, ref velocity, smooth);
    }
}
