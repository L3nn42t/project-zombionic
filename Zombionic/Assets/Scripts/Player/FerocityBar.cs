﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace PlayerAbilities
{
    public class FerocityBar : MonoBehaviour
    {
        public Slider slider;
        public Gradient gradient;
        public Image fill;

        //set speint value and maxValue to sprint variable, change slider color
        public void SetMaxSprint(float ferocity)
        {
            slider.maxValue = ferocity;
            slider.value = ferocity;

            fill.color = gradient.Evaluate(1f);
        }

        //to show sprint values in the UI sprint bar
        public void SetFerocity(float ferocity)
        {
            slider.value = ferocity;

            fill.color = gradient.Evaluate(slider.normalizedValue);
        }
    }
}
