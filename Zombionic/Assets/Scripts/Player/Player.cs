﻿using UnityEngine;
using System.Collections;

namespace PlayerAbilities
{
    public class Player : MonoBehaviour
    {
        public int maxHealth = 100;
        public int currentHealh;

        public HealthBar healthBar;

        private void Start()
        {
            currentHealh = maxHealth;
            healthBar.SetMaxHealth(maxHealth);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                TakeDamage(20);
            }
        }

        void TakeDamage(int damage)
        {
            currentHealh -= damage;
            healthBar.SetHealth(currentHealh);
        }
    }
}
