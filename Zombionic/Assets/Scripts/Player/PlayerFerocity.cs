﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlayerAbilities
{
    public class PlayerFerocity : MonoBehaviour
    {
        public PlayerMovement playerMovement;

        public bool canPlayerMove;

        private float ferocity = 100.0f;


        public FerocityBar ferocityBar;
        public float maxFerocity = 100.0f;
        public float currentFerocity;
        private bool canUseAbilities;
        private bool canKeepUsingAbilities;
        [SerializeField]
        private bool isSprinting;

        private void Start()
        {
            //set variables to values
            currentFerocity = maxFerocity;
            ferocityBar.SetMaxSprint(maxFerocity);
        }


        void Update()
        {
            //set slider value to currentSprint
            ferocityBar.SetFerocity(currentFerocity);

            // if bool false then set canSprint false
            if (!canKeepUsingAbilities)
            {
                canUseAbilities = false;

            }

            //if currentSprint >=20f set bool values true
            if (currentFerocity >= 20f)
            {
                canUseAbilities = true;
                canKeepUsingAbilities = true;
            }

            SprintChange();

            //if key pressed then set speed value to new value
            if (Input.GetKeyDown(KeyCode.LeftShift) && canUseAbilities)
            {
                playerMovement.moveSpeed = 0.8f;
                isSprinting = true;
                Debug.Log("KEYDOWN");
            }

            //if value <= 0 set value to 0 and speed to new value, set bool variables to false to not be able to sprint
            if (currentFerocity <= 0f)
            {
                currentFerocity = 0f;
                playerMovement.moveSpeed = 0.4f;
                canUseAbilities = false;
                isSprinting = false;
            }

            //if key pressed and value above 20, then bool value false
            if (Input.GetKeyDown(KeyCode.LeftShift) && currentFerocity <= 20f && canUseAbilities)
            {
                canKeepUsingAbilities = false;

            }

            //if key up set speed to value and bool variable to false
            if (Input.GetKeyUp(KeyCode.LeftShift))
            {
                playerMovement.moveSpeed = 0.4f;
                isSprinting = false;
            }
        }


        void SprintChange()
        {
            //lose sprint if key is pressed and player able to sprint
            if (Input.GetKey(KeyCode.LeftShift) && canUseAbilities && isSprinting)
            {
                currentFerocity -= 20f * Time.deltaTime;
                Debug.Log("SPRINTING");
            }

            //gain sprint if player is not sprinting
            else
            {
                currentFerocity += 4 * Time.deltaTime;
                Debug.Log("STOPPED SPRINTING");

                //set sprint value to 100 if above 100 to cap it
                if (currentFerocity >= 100f)
                {
                    currentFerocity = 100f;
                }
            }
        }
    }
}
