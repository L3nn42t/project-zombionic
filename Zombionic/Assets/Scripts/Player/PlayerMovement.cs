﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float moveSpeed;

    private Vector3 moveDirection;

    void Update()
    {
        ProcessInputs();
    }

    void FixedUpdate()
    {
        Move();

        if (Input.GetKey(KeyCode.W))
        {
            var temp = transform.rotation.eulerAngles;
            temp.y = 0.0f;
            transform.rotation = Quaternion.Euler(temp);
        }

        if (Input.GetKey(KeyCode.A))
        {
            var temp = transform.rotation.eulerAngles;
            temp.y = -90.0f;
            transform.rotation = Quaternion.Euler(temp);
        }

        if (Input.GetKey(KeyCode.S))
        {
            var temp = transform.rotation.eulerAngles;
            temp.y = 180.0f;
            transform.rotation = Quaternion.Euler(temp);
        }

        if (Input.GetKey(KeyCode.D))
        {
            var temp = transform.rotation.eulerAngles;
            temp.y = 90.0f;
            transform.rotation = Quaternion.Euler(temp);
        }
    }

    void ProcessInputs()
    {
        float moveX = Input.GetAxisRaw("Horizontal");
        float moveZ = Input.GetAxisRaw("Vertical");

        moveDirection = new Vector3(moveX, 0.0f, moveZ).normalized;
    }

    void Move()
    {
        transform.position += moveDirection * moveSpeed;
    }
}
